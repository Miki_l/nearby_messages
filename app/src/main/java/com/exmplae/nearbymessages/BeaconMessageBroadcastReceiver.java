package com.exmplae.nearbymessages;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;

public class BeaconMessageBroadcastReceiver extends BroadcastReceiver {

    public static final String SEND_MESSAGE_ACTION = "SEND_MESSAGE_ACTION";
    public static final int SEND_MESSAGE_ACTION_REQUEST_CODE = 1000;

    public static IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SEND_MESSAGE_ACTION);
        return intentFilter;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        Nearby.getMessagesClient(context).handleIntent(intent, new MessageListener() {
            @Override
            public void onFound(Message message) {
                if(message != null){
                    //DO SOMETHING
                    Toast.makeText(context, "Device is found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLost(Message message) {
                Toast.makeText(context, "Device is lost", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
