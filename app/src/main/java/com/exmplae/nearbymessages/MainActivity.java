package com.exmplae.nearbymessages;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private boolean isPublished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button publishButton = findViewById(R.id.button);
        publishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPublished) {
                    publishButton.setText(R.string.publish);
                    unpublish();
                } else {
                    publishButton.setText(R.string.unpublish);
                    publish("Hellow world");
                }

                isPublished = !isPublished;
            }
        });

        subscribe();
    }

    @Override
    protected void onStart() {
        super.onStart();
        subscribe();
    }

    @Override
    protected void onDestroy() {
        unsubscribe();
        unpublish();
        super.onDestroy();
    }

    private void subscribe() {
        BeaconIntentService.enqueueWork(getApplicationContext(), new Intent(BeaconIntentService.SUBSCRIBE_ACTION));
    }

    private void unsubscribe() {
        BeaconIntentService.enqueueWork(getApplicationContext(), new Intent(BeaconIntentService.UNSUBSCRIBE_ACTION));
    }

    private void publish(String message) {
        Intent publishIntent = new Intent(BeaconIntentService.PUBLISH_ACTION);
        Bundle bundle = new Bundle();
        bundle.putString(BeaconIntentService.PUBLISH_DATA, message);
        publishIntent.putExtras(bundle);
        BeaconIntentService.enqueueWork(getApplicationContext(), publishIntent);
    }

    private void unpublish() {
        BeaconIntentService.enqueueWork(getApplicationContext(), new Intent(BeaconIntentService.UNPUBLISH_ACTION));
    }

}
