package com.exmplae.nearbymessages;

import android.app.Application;
import android.content.Context;

public class NearbyApplication extends Application {

    private static Context appContext;

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        registerReceiver(new BeaconMessageBroadcastReceiver(), BeaconMessageBroadcastReceiver.getIntentFilter());
    }



}
