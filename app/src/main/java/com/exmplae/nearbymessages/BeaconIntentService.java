package com.exmplae.nearbymessages;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

public class BeaconIntentService extends JobIntentService {

    private static final String TAG = "BeaconIntentService";
    private static final int JOB_ID = BeaconIntentService.class.getSimpleName().hashCode(); // A unique job ID for scheduling; must be the same value for all work enqueued for the same class.
    public static final String SUBSCRIBE_ACTION = "SUBSCRIBE_ACTION";
    public static final String UNSUBSCRIBE_ACTION = "UNSUBSCRIBE_ACTION";
    public static final String PUBLISH_ACTION = "PUBLISH_ACTION";
    public static final String PUBLISH_DATA = "PUBLISH_DATA";
    public static final String UNPUBLISH_ACTION = "UNPUBLISH_ACTION";

    public static void enqueueWork(@NonNull Context context, @NonNull Intent work) {
        enqueueWork(context, BeaconIntentService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.d(TAG, "onHandleWork + " + intent);
        String action = intent.getAction();
        if (action == null) {
            Log.e(TAG, "action = null");
            return;
        }

        //noinspection IfCanBeSwitch
        if (action.equals(SUBSCRIBE_ACTION)) {
            subscribe();
        } else if (action.equals(UNSUBSCRIBE_ACTION)) {
            unsubscribe();
        } else if (action.equals(PUBLISH_ACTION)) {
            publish(intent);
        } else if (action.equals(UNPUBLISH_ACTION)) {
            unpublish();
        } else {
            Log.e(TAG, "Received unknown action: " + action);
        }

        Log.d(TAG, "onHandleWork - " + intent);
    }

    private void subscribe() {
        NearbyManager nearbyManager = NearbyManager.getInstance();
        if (!nearbyManager.isSubscribing()) {
            nearbyManager.subscribe();
        }
    }

    private void unsubscribe() {
        NearbyManager.getInstance().unsubscribe();
    }

    private void publish(Intent intent) {
        NearbyManager.getInstance().publish(intent.getExtras().getString(PUBLISH_DATA));
    }

    private void unpublish() {
        NearbyManager.getInstance().unsubscribe();
    }

}
