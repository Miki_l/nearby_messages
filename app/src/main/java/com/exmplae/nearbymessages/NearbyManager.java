package com.exmplae.nearbymessages;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.MessagesClient;
import com.google.android.gms.nearby.messages.PublishCallback;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeCallback;
import com.google.android.gms.nearby.messages.SubscribeOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.lang.ref.WeakReference;

public class NearbyManager {

    private static volatile NearbyManager INSTANCE;
    /*package*/ static NearbyManager getInstance() {
        if (INSTANCE == null) {
            synchronized (NearbyManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new NearbyManager(NearbyApplication.getAppContext());
                }
            }
        }
        return INSTANCE;
    }

    private boolean isSubscribing = false;
    private boolean isPublishing = false;
    private WeakReference<Context> context;
    private MessagesClient mMessageClient;
    private MessageListener mMessageListener;
    private static final Strategy PUB_STRATEGY = new Strategy.Builder()
            .setTtlSeconds(Strategy.TTL_SECONDS_DEFAULT).build();
    private static final Strategy SUB_STRATEGY = new Strategy.Builder()
            .setTtlSeconds(Strategy.TTL_SECONDS_MAX).build();
    private Message currentMessage;
    private PendingIntent pendingIntent;

    public boolean isSubscribing() {
        return isSubscribing;
    }

    public boolean isPublishing() {
        return isPublishing;
    }

    private NearbyManager(Context context){
        this.context = new WeakReference<>(context);
        mMessageClient = Nearby.getMessagesClient(this.context.get()/*, new MessagesOptions.Builder().setPermissions(NearbyPermissions.BLE).build()*/);
        mMessageListener = new MessageListener(){
            @Override
            public void onFound(Message message) {
                if(message != null){
                    //DO SOMETHING
                    Toast.makeText(NearbyManager.this.context.get(), "Device is found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLost(Message message) {
                Toast.makeText(NearbyManager.this.context.get(), "Device is lost", Toast.LENGTH_SHORT).show();
            }
        };
    };

    public void publish(final String message){
        PublishOptions options = new PublishOptions
                .Builder()
                .setStrategy(PUB_STRATEGY)
                .setCallback(new PublishCallback(){
                    @Override
                    public void onExpired() {
                        super.onExpired();
                        Toast.makeText(context.get(), "No longer Publishing!", Toast.LENGTH_SHORT).show();
                    }
                })
//                .setStrategy(Strategy.BLE_ONLY)
                .build();
        currentMessage = new Message(message.getBytes());
        mMessageClient.publish(currentMessage, options).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(context.get(), "Publishing! Message:" + message, Toast.LENGTH_SHORT).show();
            }
        });
        isPublishing = true;
    }

    public void subscribe(){
        SubscribeOptions options = new SubscribeOptions.Builder()
                .setStrategy(SUB_STRATEGY)
                .setCallback(new SubscribeCallback(){
                    @Override
                    public void onExpired() {
                        super.onExpired();
                        Toast.makeText(context.get(), "No longer Subscribing!", Toast.LENGTH_SHORT).show();
                    }
                })
//                .setStrategy(Strategy.BLE_ONLY)
                .build();
        pendingIntent = getPendingIntent();
        mMessageClient.subscribe(pendingIntent, options).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(context.get(), "Subscribing!", Toast.LENGTH_SHORT).show();
            }
        });

        isSubscribing = true;
    }

    private PendingIntent getPendingIntent() {
        Intent sendMessageIntent = new Intent(BeaconMessageBroadcastReceiver.SEND_MESSAGE_ACTION);
        return PendingIntent.getBroadcast(
                context.get(),
                BeaconMessageBroadcastReceiver.SEND_MESSAGE_ACTION_REQUEST_CODE,
                sendMessageIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
    }

    public void unsubscribe(){
        if(isSubscribing && mMessageClient != null)
            mMessageClient.unsubscribe(pendingIntent);
        isSubscribing = false;
    }

    public void unpublish(){
        if(isPublishing && mMessageClient != null && currentMessage != null)
            mMessageClient.unpublish(currentMessage);
        currentMessage = null;
        isPublishing = false;
    }

}
